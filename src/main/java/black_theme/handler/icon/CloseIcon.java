package black_theme.handler.icon;

import black_theme.NodeHandler;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class CloseIcon implements NodeHandler {
    Button close;
    Stage primaryStage;
    boolean[] themeFlag;
    @Override
    public void handleStyle() {
        close.setOnMouseEntered(event -> {
            // 如果主题颜色为黑色
            if(themeFlag[0]) {
                close.setStyle("-fx-text-fill: #DB6574; -fx-background-color: #333333; -fx-font-size: 15px");
            }
            // 如果主题颜色为白色
            else {
                close.setStyle("-fx-text-fill: #DB6574; -fx-background-color: #e6e6e6; -fx-font-size: 15px");
            }
        });
        close.setOnMouseExited(event -> {
            // 如果主题颜色为黑色
            if(themeFlag[0]) {
                close.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #333333; -fx-font-size: 15px");
            }
            // 如果主题颜色为白色
            else {
                close.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #e6e6e6; -fx-font-size: 15px");
            }
        });
    }

    @Override
    public void handleEvent() {
        // 点击关闭图标，则关闭窗口
        close.setOnAction(event -> primaryStage.close());
    }

    @Override
    public void handleAttributes() {

    }
    public CloseIcon(Button close, Stage primaryStage, boolean[] themeFlag) {
        this.close = close;
        this.primaryStage = primaryStage;
        this.themeFlag = themeFlag;
    }
}
