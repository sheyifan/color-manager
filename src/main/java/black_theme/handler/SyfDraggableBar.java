package black_theme.handler;

import black_theme.NodeHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

/**
 * 界面顶端可拖动的标题栏
 */

public class SyfDraggableBar implements NodeHandler {
    private Node draggableBar;
    // 需要通过stage来获取鼠标拖动的坐标点位置
    private Stage primaryStage;
    // 主题标识符，根据不同的主题定义不同的样式和事件
    private boolean[] themeFlag;
    Line obliqueLine, hLine;
    @Override
    public void handleStyle() {

    }

    @Override
    public void handleEvent() {
        final Delta delta = new Delta();
        draggableBar.setOnMousePressed(event -> {
            delta.x = primaryStage.getX() - event.getScreenX();
            delta.y = primaryStage.getY() - event.getScreenY();

            // draggableBar.setCursor(Cursor.CLOSED_HAND);
        });
        draggableBar.setOnMouseDragged(event -> {
            primaryStage.setX(event.getScreenX() + delta.x);
            primaryStage.setY(event.getScreenY() + delta.y);
        });
    }

    @Override
    public void handleAttributes() {

    }
    public SyfDraggableBar(Node draggableBar, Stage primaryStage, boolean[] themeFlag, Line obliqueLine, Line hLine) {
        this.draggableBar = draggableBar;
        this.primaryStage = primaryStage;
        this.themeFlag = themeFlag;
        this.obliqueLine = obliqueLine;
        this.hLine = hLine;
    }
}
