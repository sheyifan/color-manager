package black_theme;

/**
 * 处理界面中各节点的：
 *      动态样式；
 *      节点属性；
 *      事件；
 */

public interface NodeHandler {
    // 设置节点的动态样式。包括事件触发的样式变化，以及动画效果
    public abstract void handleStyle();
    // 设置节点的事件
    public abstract void handleEvent();
    // 设置节点的属性
    public abstract void handleAttributes();
}
