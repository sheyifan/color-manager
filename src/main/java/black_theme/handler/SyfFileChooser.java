package black_theme.handler;

import black_theme.NodeHandler;
import javafx.stage.FileChooser;

/**
 * 组件：文件选择
 */

public class SyfFileChooser implements NodeHandler {
    private FileChooser fileChooser;
    @Override
    public void handleStyle() {

    }

    @Override
    public void handleEvent() {

    }

    @Override
    public void handleAttributes() {
        // 添加后缀名过滤器
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("all images", "*.jpg", "*.bmp", "*.gif", "*.jpeg", "*.png"),
                new FileChooser.ExtensionFilter("jpg", "*.jpg"),
                new FileChooser.ExtensionFilter("jpeg", "*.jpeg"),
                new FileChooser.ExtensionFilter("gif", "*.gif"),
                new FileChooser.ExtensionFilter("bmp", "*.bmp"),
                new FileChooser.ExtensionFilter("png", "*.png")
        );
    }
    public SyfFileChooser(FileChooser fileChooser) {
        this.fileChooser = fileChooser;
    }
}
