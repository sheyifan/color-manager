package black_theme.handler;

import black_theme.NodeHandler;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;

public class MyColorPicker implements NodeHandler {
    ColorPicker chooseButton;
    Slider RSlider, GSlider, BSlider;
    @Override
    public void handleStyle() {

    }

    @Override
    public void handleEvent() {
        chooseButton.setOnAction(event -> {
            String color16 = chooseButton.getValue().toString();
            String red16;
            red16 = color16.substring(2, 4);
            String green16;
            green16 = color16.substring(4, 6);
            String blue16 = color16.substring(6, 8);
            int redValue = Integer.valueOf(red16, 16);
            int greenValue = Integer.valueOf(green16, 16);
            int blueValue = Integer.valueOf(blue16, 16);

            // 颜色选定之后，改变RSlider, GSlider, BSlider的值。
            RSlider.setValue(redValue);
            GSlider.setValue(greenValue);
            BSlider.setValue(blueValue);
        });
    }

    @Override
    public void handleAttributes() {
        // 给颜色选择按钮赋初值
        chooseButton.setValue(Color.BLACK);
    }
    public MyColorPicker(ColorPicker chooseButton, Slider RSlider, Slider GSlider, Slider BSlider) {
        this.chooseButton = chooseButton;
        this.RSlider = RSlider;
        this.GSlider = GSlider;
        this.BSlider = BSlider;
    }
}
