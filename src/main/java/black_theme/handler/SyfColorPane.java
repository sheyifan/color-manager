package black_theme.handler;

import black_theme.NodeHandler;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.Pane;

/**
 * 界面中央显示颜色的颜色块
 */

public class SyfColorPane implements NodeHandler {
    Pane colorPane;
    @Override
    public void handleStyle() {
        // 设置点击颜色板时的边框阴影，同时将颜色版的颜色复制到剪贴板
        colorPane.setOnMousePressed(event -> colorPane.setEffect(new InnerShadow()));
        colorPane.setOnMouseReleased(event -> {
            colorPane.setEffect(null);
            String color = colorPane.getBackground().getFills().get(0).getFill().toString();
            Clipboard clipboard = Clipboard.getSystemClipboard();
            ClipboardContent clipboardContent = new ClipboardContent();
            clipboardContent.putString(color.substring(2));
            clipboard.setContent(clipboardContent);
        });
    }

    @Override
    public void handleEvent() {

    }

    @Override
    public void handleAttributes() {

    }
    public SyfColorPane(Pane colorPane) {
        this.colorPane = colorPane;
    }
}
