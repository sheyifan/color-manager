package black_theme.handler.icon;

import black_theme.NodeHandler;
import black_theme.handler.StartAlert;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;

public class InfoIcon implements NodeHandler {
    private ImageView info;
    private boolean[] themeFlag;
    @Override
    public void handleStyle() {

    }

    @Override
    public void handleEvent() {
        info.setOnMousePressed(event -> info.setFitHeight(18));
        info.setOnMouseReleased(event -> {
            info.setFitHeight(20);
            //建立弹窗，显示软件的具体信息
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            // 处理打开软件时的弹出的信息窗口
            NodeHandler startAlert = new StartAlert(alert, themeFlag);
            startAlert.handleStyle();
            startAlert.handleAttributes();
            startAlert.handleEvent();
            // 显示弹窗。这里会等待用户确认。
            alert.showAndWait();
        });
    }

    @Override
    public void handleAttributes() {

    }
    public InfoIcon(ImageView info, boolean[] themeFlag) {
        this.info = info;
        this.themeFlag = themeFlag;
    }
}
