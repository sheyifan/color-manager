package black_theme.handler;

/**
 * 该类用于记录自定义可拖动的标题栏的拖动数值
 */
public class Delta {
    double x;
    double y;
}
