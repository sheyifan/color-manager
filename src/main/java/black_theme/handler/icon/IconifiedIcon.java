package black_theme.handler.icon;

import black_theme.NodeHandler;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class IconifiedIcon implements NodeHandler {
    Button iconified;
    Stage primaryStage;
    boolean[] themeFlag;
    @Override
    public void handleStyle() {
        iconified.setOnMouseEntered(event -> {
            // 如果主题颜色为黑色
            if(themeFlag[0]) {
                iconified.setStyle("-fx-text-fill: #DB6574; -fx-background-color: #333333; -fx-font-size: 15px");
            }
            // 如果主题颜色为白色
            else {
                iconified.setStyle("-fx-text-fill: #DB6574; -fx-background-color: #e6e6e6; -fx-font-size: 15px");
            }
        });
        iconified.setOnMouseExited(event -> {
            // 如果主题颜色为黑色
            if(themeFlag[0]) {
                iconified.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #333333; -fx-font-size: 15px");
            }
            // 如果主题颜色为白色
            else {
                iconified.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #e6e6e6; -fx-font-size: 15px");
            }
        });
    }

    @Override
    public void handleEvent() {
        iconified.setOnAction(event -> primaryStage.setIconified(true));
    }

    @Override
    public void handleAttributes() {

    }
    public IconifiedIcon(Button iconified, Stage primaryStage, boolean[] themeFlag) {
        this.iconified = iconified;
        this.primaryStage = primaryStage;
        this.themeFlag = themeFlag;
    }
}
