package black_theme.handler.color_model;

import black_theme.NodeHandler;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

public class SyfSliderText implements NodeHandler {
    // 滚动条
    private Slider RSlider, GSlider, BSlider, HSlider, SSlider, VSlider, OSlider;
    // 滚动条对应的显示数值
    private TextField RText, GText, BText, HText, SText, VText, OText;
    @Override
    public void handleStyle() {

    }

    @Override
    public void handleEvent() {
        RTextEvent();
        GTextEvent();
        BTextEvent();
        HTextEvent();
        STextEvent();
        VTextEvent();
        OTextEvent();
    }

    @Override
    public void handleAttributes() {

    }
    private void RTextEvent() {
        RText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] RTextChars = newValue.toCharArray();
            boolean flag = true;
            if(RTextChars.length == 0)
                flag = false;
            else{
                for(char s : RTextChars)
                {
                    if(s<'0' || s>'9') {
                        flag = false;
                        RText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                int RValue = Integer.parseInt(newValue);
                if(RValue>=0 && RValue<=255)
                    RSlider.setValue(RValue);
                else{
                    RText.setText(oldValue);
                    RSlider.setValue(Integer.parseInt(oldValue));
                }
            }
        });
        RText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
            {
                if(RText.getText().equals(""))
                    RText.setText("0");
            }
        });
    }
    private void GTextEvent() {
        GText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] GTextChars = newValue.toCharArray();
            boolean flag = true;
            if(GTextChars.length == 0)
                flag = false;
            else{
                for(char s : GTextChars)
                {
                    if(s<'0' || s>'9') {
                        flag = false;
                        GText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                int GValue = Integer.parseInt(newValue);
                if(GValue>=0 && GValue<=255)
                    GSlider.setValue(GValue);
                else{
                    GText.setText(oldValue);
                    GSlider.setValue(Integer.parseInt(oldValue));
                }
            }
        });
        GText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
            {
                if(GText.getText().equals(""))
                    GText.setText("0");
            }
        });
    }
    private void BTextEvent() {
        BText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] BTextChars = newValue.toCharArray();
            boolean flag = true;
            if(BTextChars.length == 0)
                flag = false;
            else{
                for(char s : BTextChars)
                {
                    if(s<'0' || s>'9') {
                        flag = false;
                        BText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                int BValue = Integer.parseInt(newValue);
                if(BValue>=0 && BValue<=255)
                    BSlider.setValue(BValue);
                else{
                    BText.setText(oldValue);
                    BSlider.setValue(Integer.parseInt(oldValue));
                }
            }
        });
        BText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
            {
                if(BText.getText().equals(""))
                    BText.setText("0");
            }
        });
    }
    private void HTextEvent() {
        HText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] HTextChars = newValue.toCharArray();
            boolean flag = true;
            if(HTextChars.length == 0)
                flag = false;
            else{
                for(char s : HTextChars)
                {
                    if(s<'0' || s>'9') {
                        flag = false;
                        HText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                int HValue = Integer.parseInt(newValue);
                if(HValue>=0 && HValue<=360)
                    HSlider.setValue(HValue);
                else{
                    HText.setText(oldValue);
                    HSlider.setValue(Integer.parseInt(oldValue));
                }
            }
        });
        HText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
            {
                if(HText.getText().equals(""))
                    HText.setText("0");
            }
        });
    }
    private void STextEvent() {
        SText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] STextChars = newValue.toCharArray();
            boolean flag = true;
            if(STextChars.length == 0)
                flag = false;
            else{
                for(char s : STextChars)
                {
                    if(s<'0' || s>'9') {
                        flag = false;
                        SText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                int SValue = Integer.parseInt(newValue);
                if(SValue>=0 && SValue<=100)
                    SSlider.setValue(SValue);
                else{
                    SText.setText(oldValue);
                    SSlider.setValue(Integer.parseInt(oldValue));
                }
            }
        });
        SText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
            {
                if(SText.getText().equals(""))
                    SText.setText("0");
            }
        });
    }
    private void VTextEvent() {
        VText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] VTextChars = newValue.toCharArray();
            boolean flag = true;
            if(VTextChars.length == 0)
                flag = false;
            else{
                for(char s : VTextChars)
                {
                    if(s<'0' || s>'9') {
                        flag = false;
                        VText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                int VValue = Integer.parseInt(newValue);
                if(VValue>=0 && VValue<=100)
                    VSlider.setValue(VValue);
                else{
                    VText.setText(oldValue);
                    VSlider.setValue(Integer.parseInt(oldValue));
                }
            }
        });
        VText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
            {
                if(VText.getText().equals(""))
                    VText.setText("0");
            }
        });
    }
    private void OTextEvent() {
        OText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] OTextChars = newValue.toCharArray();
            boolean flag = true;
            if(OTextChars.length == 0)
                flag = false;
            else{
                for(char s : OTextChars)
                {
                    if(s<'0' || s>'9') {
                        flag = false;
                        OText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                int OValue = Integer.parseInt(newValue);
                if(OValue>=0 && OValue<=255)
                    OSlider.setValue(OValue);
                else{
                    OText.setText(oldValue);
                    OSlider.setValue(Integer.parseInt(oldValue));
                }
            }
        });
        OText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
            {
                if(OText.getText().equals(""))
                    OText.setText("0");
            }
        });
    }
    public SyfSliderText(Slider RSlider,
                     Slider GSlider,
                     Slider BSlider,
                     Slider HSlider,
                     Slider SSlider,
                     Slider VSlider,
                     Slider OSlider,
                     TextField RText,
                     TextField GText,
                     TextField BText,
                     TextField HText,
                     TextField SText,
                     TextField VText,
                     TextField OText) {
        this.RSlider = RSlider;
        this.GSlider = GSlider;
        this.BSlider = BSlider;
        this.HSlider = HSlider;
        this.SSlider = SSlider;
        this.VSlider = VSlider;
        this.OSlider = OSlider;

        this.RText = RText;
        this.GText = GText;
        this.BText = BText;
        this.HText = HText;
        this.SText = SText;
        this.VText = VText;
        this.OText = OText;
    }
}
