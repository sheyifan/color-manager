package black_theme.handler.color_model;

import black_theme.NodeHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;


/**
 * 6位或者8位16进制文本框
 */

public class SyfWebText implements NodeHandler {
    private TextField webText;
    // 滚动条
    private Slider RSlider, GSlider, BSlider, HSlider, SSlider, VSlider, OSlider;

    @Override
    public void handleStyle() {

    }

    @Override
    public void handleEvent() {
        webText.textProperty().addListener((observable, oldValue, newValue) -> {
            char[] webTextChars = newValue.toCharArray();
            boolean flag = true;
            if(webTextChars.length == 0)
                flag = false;
            if(webTextChars.length == 9)
                webText.setText(oldValue);
            else{
                for(char s : webTextChars)
                {
                    if(s>='0' && s<='9' || s>='a' && s<='f')
                    {
                        System.out.print("");
                    }
                    else{
                        flag = false;
                        webText.setText(oldValue);
                        break;
                    }
                }
            }
            if(flag)
            {
                if(webTextChars.length==6 || webTextChars.length==8) {
                    if(webTextChars.length == 6)
                    {
                        String R16 = newValue.substring(0, 2);
                        String G16 = newValue.substring(2, 4);
                        String B16 = newValue.substring(4, 6);
                        RSlider.setValue(Integer.valueOf(R16, 16));
                        GSlider.setValue(Integer.valueOf(G16, 16));
                        BSlider.setValue(Integer.valueOf(B16, 16));
                    }
                    else{
                        String R16 = newValue.substring(0, 2);
                        String G16 = newValue.substring(2, 4);
                        String B16 = newValue.substring(4, 6);
                        String O16 = newValue.substring(6);
                        RSlider.setValue(Integer.valueOf(R16, 16));
                        GSlider.setValue(Integer.valueOf(G16, 16));
                        BSlider.setValue(Integer.valueOf(B16, 16));
                        OSlider.setValue(Integer.valueOf(O16, 16));
                    }
                }
            }
        });
        // webText失去焦点的时候，如果数值为6位，那么将颜色的透明度数值恢复为255。
        webText.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue) {
                char[] webTextChars = webText.getText().toCharArray();
                if(webTextChars.length == 6) {
                    OSlider.setValue(255);
                }
            }
        });
    }

    @Override
    public void handleAttributes() {

    }
    public SyfWebText(TextField webText,
                      Slider RSlider,
                      Slider GSlider,
                      Slider BSlider,
                      Slider HSlider,
                      Slider SSlider,
                      Slider VSlider,
                      Slider OSlider) {
        this.webText = webText;
        this.RSlider = RSlider;
        this.GSlider = GSlider;
        this.BSlider = BSlider;
        this.HSlider = HSlider;
        this.SSlider = SSlider;
        this.VSlider = VSlider;
        this.OSlider = OSlider;
    }
}
