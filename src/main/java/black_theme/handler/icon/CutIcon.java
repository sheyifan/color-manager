package black_theme.handler.icon;

import black_theme.NodeHandler;
import javafx.scene.image.ImageView;

import java.awt.Robot;
import java.awt.AWTException;
import java.awt.event.KeyEvent;


public class CutIcon implements NodeHandler {
    private ImageView cut;
    @Override
    public void handleStyle() {

    }

    @Override
    public void handleEvent() {
        cut.setOnMousePressed(event -> {
            cut.setFitHeight(19);
//            try{
//                Robot robot = new Robot();
//                robot.keyPress(KeyEvent.VK_SHIFT);
//                robot.keyPress(KeyEvent.VK_META);
//                robot.keyPress(KeyEvent.VK_4);
//                robot.keyRelease(KeyEvent.VK_4);
//                robot.keyRelease(KeyEvent.VK_META);
//                robot.keyRelease(KeyEvent.VK_SHIFT);
//            }catch(AWTException ae){
//                ae.printStackTrace();
//            }
        });
        cut.setOnMouseReleased(event -> cut.setFitHeight(21));
    }

    @Override
    public void handleAttributes() {

    }
    public CutIcon(ImageView cut) {
        this.cut = cut;
    }
}
