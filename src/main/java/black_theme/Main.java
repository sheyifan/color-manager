package black_theme;

import black_theme.handler.*;
import black_theme.handler.color_model.SyfSlider;
import black_theme.handler.color_model.SyfSliderText;
import black_theme.handler.color_model.SyfWebText;
import black_theme.handler.icon.*;
import com.sun.javafx.stage.StageHelper;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;


public class Main extends Application {
    // RGB和HSV模型相互转换的中间过程中，颜色模型值需要乘上的倍数。这样有利于高精度转换。
    // public static final double transferRatio = 1000000;

    @Override
    public void start(Stage primaryStage) throws Exception{
        // 设置根结点
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("black.fxml")));
        primaryStage.setTitle("调色板");
        // 界面的位置
        primaryStage.setX(350);
        primaryStage.setY(250);
        // 界面大小
        Scene scene = new Scene(root, 248, 454);
        // 将界面设置为透明。这不会影响节点的透明度.
        scene.setFill(null);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);



        // 这是一个标志位，用来标识当前的主题颜色(true代表黑色，false代表白色)
        final boolean[] themeFlag = {true};
        // 获取所有的窗口Stage
        ObservableList<Stage> stages = StageHelper.getStages();


        // 根结点pane
        Pane pane = (Pane)root.lookup("#pane");
        // 获取标题栏对象
        Pane titleBar = (Pane)root.lookup("#titleBar");
        // 获取标题栏可拖动部分的对象
        Canvas draggableBar = (Canvas) root.lookup("#draggable-bar");
        // 标题栏中关闭和最小化的按钮
        Button closeButton = (Button)root.lookup("#close");
        Button iconifiedButton = (Button)root.lookup("#iconified");
        // 标题栏右侧的拖动区域标识线
        Line obliqueLine = (Line) root.lookup("#oblique-line");
        Line hLine = (Line) root.lookup("#h-line");
        // 界面中央的分隔符
        Separator middleSeparator = (Separator) root.lookup("#middle-separator");


        // Parent root = FXMLLoader.load(getClass().getResource("black.fxml"));
        // 在Maven项目中加载资源文件需要采取下面的写法
        PieChart pieChart = (PieChart)root.lookup("#pieChart");
        // 生成被监听的列表ObservableList
        ObservableList<PieChart.Data> chartData = FXCollections.observableArrayList(
                new PieChart.Data("R", 0.33333),
                new PieChart.Data("G", 0.33333),
                new PieChart.Data("B", 0.33333)
        );


        // 处理多标签窗口对象
        TabPane tabPane = (TabPane)root.lookup("#tabPane");
        // 设置标签位于窗口的位置
        tabPane.setSide(Side.TOP);


        // 滚动条
        Slider RSlider = (Slider)root.lookup("#RSlider");
        Slider GSlider = (Slider)root.lookup("#GSlider");
        Slider BSlider = (Slider)root.lookup("#BSlider");
        Slider HSlider = (Slider)root.lookup("#HSlider");
        Slider SSlider = (Slider)root.lookup("#SSlider");
        Slider VSlider = (Slider)root.lookup("#VSlider");
        Slider OSlider = (Slider)root.lookup("#OSlider");
        // 滚动条对应的数值
        TextField RText = (TextField)root.lookup("#RText");
        TextField GText = (TextField)root.lookup("#GText");
        TextField BText = (TextField)root.lookup("#BText");
        TextField HText = (TextField)root.lookup("#HText");
        TextField SText = (TextField)root.lookup("#SText");
        TextField VText = (TextField)root.lookup("#VText");
        TextField OText = (TextField)root.lookup("#OText");
        // 滚动条数值的单位。具体来说是HSV模型的单位。RGB模型数值没有单位。
        Label SSLabel = (Label)root.lookup("#SSLabel");
        SSLabel.setText("%");
        SSLabel.setStyle("-fx-text-fill: #8b8d9388");
        Label SVLabel = (Label)root.lookup("#SVLabel");
        SVLabel.setText("%");
        SVLabel.setStyle("-fx-text-fill: #8b8d9388");
        Label SHLabel = (Label)root.lookup("#SHLabel");
        SHLabel.setText("°");
        SHLabel.setStyle("-fx-text-fill: #8b8d9388");


        // 用来显示和控制16进制RGB值的文本框
        TextField webText = (TextField)root.lookup("#webText");
        // 用来显示当前颜色的容器
        Pane colorPane = (Pane)root.lookup("#colorPane");
        // 选择颜色的按钮
        ColorPicker chooseButton = (ColorPicker)root.lookup("#chooseButton");


        // 选择文件的组件
        FileChooser fileChooser = new FileChooser();
        SyfFileChooser syfFileChooser = new SyfFileChooser(fileChooser);
        syfFileChooser.handleAttributes();
        syfFileChooser.handleEvent();
        syfFileChooser.handleStyle();
        // 截图、拍照、打开文件和主题颜色的图标
        ImageView cut = (ImageView)root.lookup("#cut");
        ImageView photo = (ImageView)root.lookup("#photo");
        ImageView file = (ImageView)root.lookup("#file");
        final ImageView[] theme = {(ImageView) root.lookup("#theme")};
        ImageView info = (ImageView) root.lookup("#info");
        // 放大镜图标
        ImageView biggerIcon = (ImageView) root.lookup("#bigger_icon");


        Pane pixelBox = (Pane) root.lookup("#pixel-box");
        // 对显示在屏幕上的像素点进行裁剪
        pixelBox.setClip(new Circle(45, 45, 45));
        // 获取放大镜的所有像素点对象。
        Pane p0p4 = (Pane)root.lookup("#p0p4");
        Pane p0p3 = (Pane)root.lookup("#p0p3");
        Pane p0p2 = (Pane)root.lookup("#p0p2");
        Pane p0p1 = (Pane)root.lookup("#p0p1");
        Pane p0p0 = (Pane)root.lookup("#p0p0");
        Pane p0n1 = (Pane)root.lookup("#p0n1");
        Pane p0n2 = (Pane)root.lookup("#p0n2");
        Pane p0n3 = (Pane)root.lookup("#p0n3");
        Pane p0n4 = (Pane)root.lookup("#p0n4");

        Pane n1p4 = (Pane)root.lookup("#n1p4");
        Pane n1p3 = (Pane)root.lookup("#n1p3");
        Pane n1p2 = (Pane)root.lookup("#n1p2");
        Pane n1p1 = (Pane)root.lookup("#n1p1");
        Pane n1p0 = (Pane)root.lookup("#n1p0");
        Pane n1n1 = (Pane)root.lookup("#n1n1");
        Pane n1n2 = (Pane)root.lookup("#n1n2");
        Pane n1n3 = (Pane)root.lookup("#n1n3");
        Pane n1n4 = (Pane)root.lookup("#n1n4");

        Pane p1p4 = (Pane)root.lookup("#p1p4");
        Pane p1p3 = (Pane)root.lookup("#p1p3");
        Pane p1p2 = (Pane)root.lookup("#p1p2");
        Pane p1p1 = (Pane)root.lookup("#p1p1");
        Pane p1p0 = (Pane)root.lookup("#p1p0");
        Pane p1n1 = (Pane)root.lookup("#p1n1");
        Pane p1n2 = (Pane)root.lookup("#p1n2");
        Pane p1n3 = (Pane)root.lookup("#p1n3");
        Pane p1n4 = (Pane)root.lookup("#p1n4");

        Pane n2p4 = (Pane)root.lookup("#n2p4");
        Pane n2p3 = (Pane)root.lookup("#n2p3");
        Pane n2p2 = (Pane)root.lookup("#n2p2");
        Pane n2p1 = (Pane)root.lookup("#n2p1");
        Pane n2p0 = (Pane)root.lookup("#n2p0");
        Pane n2n1 = (Pane)root.lookup("#n2n1");
        Pane n2n2 = (Pane)root.lookup("#n2n2");
        Pane n2n3 = (Pane)root.lookup("#n2n3");
        Pane n2n4 = (Pane)root.lookup("#n2n4");

        Pane p2p4 = (Pane)root.lookup("#p2p4");
        Pane p2p3 = (Pane)root.lookup("#p2p3");
        Pane p2p2 = (Pane)root.lookup("#p2p2");
        Pane p2p1 = (Pane)root.lookup("#p2p1");
        Pane p2p0 = (Pane)root.lookup("#p2p0");
        Pane p2n1 = (Pane)root.lookup("#p2n1");
        Pane p2n2 = (Pane)root.lookup("#p2n2");
        Pane p2n3 = (Pane)root.lookup("#p2n3");
        Pane p2n4 = (Pane)root.lookup("#p2n4");

        Pane n3p4 = (Pane)root.lookup("#n3p4");
        Pane n3p3 = (Pane)root.lookup("#n3p3");
        Pane n3p2 = (Pane)root.lookup("#n3p2");
        Pane n3p1 = (Pane)root.lookup("#n3p1");
        Pane n3p0 = (Pane)root.lookup("#n3p0");
        Pane n3n1 = (Pane)root.lookup("#n3n1");
        Pane n3n2 = (Pane)root.lookup("#n3n2");
        Pane n3n3 = (Pane)root.lookup("#n3n3");
        Pane n3n4 = (Pane)root.lookup("#n3n4");

        Pane p3p4 = (Pane)root.lookup("#p3p4");
        Pane p3p3 = (Pane)root.lookup("#p3p3");
        Pane p3p2 = (Pane)root.lookup("#p3p2");
        Pane p3p1 = (Pane)root.lookup("#p3p1");
        Pane p3p0 = (Pane)root.lookup("#p3p0");
        Pane p3n1 = (Pane)root.lookup("#p3n1");
        Pane p3n2 = (Pane)root.lookup("#p3n2");
        Pane p3n3 = (Pane)root.lookup("#p3n3");
        Pane p3n4 = (Pane)root.lookup("#p3n4");

        Pane n4p4 = (Pane)root.lookup("#n4p4");
        Pane n4p3 = (Pane)root.lookup("#n4p3");
        Pane n4p2 = (Pane)root.lookup("#n4p2");
        Pane n4p1 = (Pane)root.lookup("#n4p1");
        Pane n4p0 = (Pane)root.lookup("#n4p0");
        Pane n4n1 = (Pane)root.lookup("#n4n1");
        Pane n4n2 = (Pane)root.lookup("#n4n2");
        Pane n4n3 = (Pane)root.lookup("#n4n3");
        Pane n4n4 = (Pane)root.lookup("#n4n4");

        Pane p4p4 = (Pane)root.lookup("#p4p4");
        Pane p4p3 = (Pane)root.lookup("#p4p3");
        Pane p4p2 = (Pane)root.lookup("#p4p2");
        Pane p4p1 = (Pane)root.lookup("#p4p1");
        Pane p4p0 = (Pane)root.lookup("#p4p0");
        Pane p4n1 = (Pane)root.lookup("#p4n1");
        Pane p4n2 = (Pane)root.lookup("#p4n2");
        Pane p4n3 = (Pane)root.lookup("#p4n3");
        Pane p4n4 = (Pane)root.lookup("#p4n4");







        // 下面开始处理对象树中组件的attributes、动态样式以及其他事件

        // 处理饼图
        NodeHandler pieChartHandler = new MyChart(pieChart, chartData);
        pieChartHandler.handleAttributes();
        pieChartHandler.handleStyle();
        pieChartHandler.handleEvent();
        // 处理可拖动标题栏和可拖动界面
        NodeHandler syfDraggableBar = new SyfDraggableBar(draggableBar, primaryStage, themeFlag, obliqueLine, hLine);
        syfDraggableBar.handleAttributes();
        syfDraggableBar.handleEvent();
        syfDraggableBar.handleStyle();
        NodeHandler syfDraggableWindow = new SyfDraggableBar(pane, primaryStage, themeFlag, obliqueLine, hLine);
        syfDraggableWindow.handleAttributes();
        syfDraggableWindow.handleEvent();
        syfDraggableWindow.handleStyle();
        // 处理窗口关闭图标
        NodeHandler closeIcon = new CloseIcon(closeButton, primaryStage, themeFlag);
        closeIcon.handleAttributes();
        closeIcon.handleEvent();
        closeIcon.handleStyle();
        // 处理最小化图标
        NodeHandler iconifiedIcon = new IconifiedIcon(iconifiedButton, primaryStage, themeFlag);
        iconifiedIcon.handleAttributes();
        iconifiedIcon.handleEvent();
        iconifiedIcon.handleStyle();
        // 处理截图图标
        NodeHandler cutIcon = new CutIcon(cut);
        cutIcon.handleStyle();
        cutIcon.handleEvent();
        cutIcon.handleAttributes();
        // 处理照相图标
        NodeHandler photoIcon = new PhotoIcon(photo);
        photoIcon.handleAttributes();
        photoIcon.handleEvent();
        photoIcon.handleStyle();
        // 处理信息图标
        NodeHandler infoIcon = new InfoIcon(info, themeFlag);
        infoIcon.handleStyle();
        infoIcon.handleEvent();
        infoIcon.handleAttributes();
        // 处理界面中央的演示显示板
        NodeHandler syfColorPane = new SyfColorPane(colorPane);
        syfColorPane.handleAttributes();
        syfColorPane.handleEvent();
        syfColorPane.handleStyle();
        // 处理颜色选择框
        NodeHandler myColorPicker = new MyColorPicker(chooseButton, RSlider, GSlider, BSlider);
        myColorPicker.handleAttributes();
        myColorPicker.handleEvent();
        myColorPicker.handleStyle();
        // 处理Slider文本框的监听。文本框值改变的时候将会导致Slider也发生改变
        NodeHandler syfSliderText = new SyfSliderText(
                RSlider, GSlider, BSlider, HSlider, SSlider, VSlider, OSlider,
                RText, GText, BText, HText, SText, VText, OText);
        syfSliderText.handleStyle();
        syfSliderText.handleEvent();
        syfSliderText.handleAttributes();
        // 处理16进制颜色数值文本框的数值监听
        NodeHandler syfWebText = new SyfWebText(webText, RSlider, GSlider, BSlider, HSlider, SSlider, VSlider, OSlider);
        syfWebText.handleAttributes();
        syfWebText.handleEvent();
        syfWebText.handleStyle();
        // 处理滚动条的监听
        NodeHandler syfSlider = new SyfSlider(
                RSlider, GSlider, BSlider, HSlider, SSlider, VSlider, OSlider,
                RText, GText, BText, HText, SText, VText, OText,
                pieChart, chartData, webText, colorPane);
        syfSlider.handleAttributes();
        syfSlider.handleEvent();
        syfSlider.handleStyle();
        // 处理file图标
        file.setOnMousePressed(event -> file.setFitHeight(16));
        file.setOnMouseReleased(event -> file.setFitHeight(18));
        file.setOnMouseClicked(event -> {
            int stageQuantity = stages.size();
            if(stageQuantity == 1)
            {
                File path = fileChooser.showOpenDialog(primaryStage);
                if(path == null)
                {
                    System.err.println("图片打开失败。\n");
                }
                else{
                    System.out.println(path);
                    try{
                        primaryStage.setX(10);
                        primaryStage.setY(250);
                        FileInputStream inFile = new FileInputStream(path);
                        Image image = new Image(inFile);
                        double width = image.getWidth();
                        double height = image.getHeight();
                        if(width>750.0)
                            System.err.println("图片宽度超标，建议缩放至750px之内再进行处理。");
                        if(height>900.0)
                            System.err.println("图片高度超标，建议缩放至900px之内再进行处理。");
                        System.out.println("图片宽度："+ width+ "; 图片高度：" + height + ";");
                        // Parent children = FXMLLoader.load(getClass().getResource("children.fxml"));
                        Parent children = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("children.fxml")));
                        javafx.scene.image.ImageView imageView = (javafx.scene.image.ImageView)children.lookup("#imageView");
                        imageView.setImage(image);
                        imageView.setFitWidth(width);
                        imageView.setFitHeight(height);
                        Stage childStage = new Stage();
                        Scene childScene = new Scene(children, width, height);
                        childStage.setScene(childScene);
                        childStage.setResizable(false);
                        childStage.initStyle(StageStyle.UTILITY);


                        childStage.initOwner(primaryStage);
                        childStage.show();
                        childStage.setX(260);
                        childStage.setY(10);


                        childStage.showingProperty().addListener((observable, oldValue, newValue) -> {
                            if(!newValue) {
                                primaryStage.setX(350);
                                primaryStage.setY(250);
                            }
                        });

                        imageView.setOnMouseDragged(event1 -> {
                            double x = event1.getX();
                            double y = event1.getY();
                            String color = imageView.getImage().getPixelReader().getColor((int)x, (int)y).toString();
                            int red = Integer.valueOf(color.substring(2, 4), 16);
                            int green = Integer.valueOf(color.substring(4, 6), 16);
                            int blue = Integer.valueOf(color.substring(6, 8), 16);
                            int opacity = Integer.valueOf(color.substring(8), 16);
                            RSlider.setValue(red);
                            GSlider.setValue(green);
                            BSlider.setValue(blue);
                            OSlider.setValue(opacity);
                            Pixels pixels = new Pixels(
                                    p0p4, p0p3, p0p2, p0p1, p0p0, p0n1, p0n2, p0n3, p0n4,

                                    n1p4, n1p3, n1p2, n1p1, n1p0, n1n1, n1n2, n1n3, n1n4,
                                    p1p4, p1p3, p1p2, p1p1, p1p0 ,p1n1, p1n2, p1n3, p1n4,

                                    n2p4, n2p3, n2p2, n2p1, n2p0, n2n1, n2n2, n2n3, n2n4,
                                    p2p4, p2p3, p2p2, p2p1, p2p0, p2n1, p2n2, p2n3, p2n4,

                                    n3p4, n3p3, n3p2, n3p1, n3p0, n3n1, n3n2, n3n3, n3n4,
                                    p3p4, p3p3, p3p2, p3p1, p3p0, p3n1, p3n2, p3n3, p3n4,

                                    n4p4, n4p3, n4p2, n4p1, n4p0, n4n1, n4n2, n4n3, n4n4,
                                    p4p4, p4p3, p4p2, p4p1, p4p0, p4n1, p4n2, p4n3, p4n4
                            );
                            pixels.paint(imageView, (int)x, (int)y);
                        });
                        imageView.setOnMousePressed(event1 -> {
                            imageView.setEffect(new InnerShadow());
                            double x = event1.getX();
                            double y = event1.getY();
                            String color = imageView.getImage().getPixelReader().getColor((int)x, (int)y).toString();
                            int red = Integer.valueOf(color.substring(2, 4), 16);
                            int green = Integer.valueOf(color.substring(4, 6), 16);
                            int blue = Integer.valueOf(color.substring(6, 8), 16);
                            int opacity = Integer.valueOf(color.substring(8), 16);
                            RSlider.setValue(red);
                            GSlider.setValue(green);
                            BSlider.setValue(blue);
                            OSlider.setValue(opacity);
                            Pixels pixels = new Pixels(
                                    p0p4, p0p3, p0p2, p0p1, p0p0, p0n1, p0n2, p0n3, p0n4,

                                    n1p4, n1p3, n1p2, n1p1, n1p0, n1n1, n1n2, n1n3, n1n4,
                                    p1p4, p1p3, p1p2, p1p1, p1p0 ,p1n1, p1n2, p1n3, p1n4,

                                    n2p4, n2p3, n2p2, n2p1, n2p0, n2n1, n2n2, n2n3, n2n4,
                                    p2p4, p2p3, p2p2, p2p1, p2p0, p2n1, p2n2, p2n3, p2n4,

                                    n3p4, n3p3, n3p2, n3p1, n3p0, n3n1, n3n2, n3n3, n3n4,
                                    p3p4, p3p3, p3p2, p3p1, p3p0, p3n1, p3n2, p3n3, p3n4,

                                    n4p4, n4p3, n4p2, n4p1, n4p0, n4n1, n4n2, n4n3, n4n4,
                                    p4p4, p4p3, p4p2, p4p1, p4p0, p4n1, p4n2, p4n3, p4n4
                            );
                            pixels.paint(imageView, (int)x, (int)y);
                        });
                        imageView.setOnMouseReleased(event1 -> {
                            imageView.setEffect(null);
                            double x = event1.getX();
                            double y = event1.getY();
                            String color = imageView.getImage().getPixelReader().getColor((int)x, (int)y).toString();
                            Clipboard clipboard = Clipboard.getSystemClipboard();
                            ClipboardContent clipboardContent = new ClipboardContent();
                            clipboardContent.putString(color.substring(2));
                            clipboard.setContent(clipboardContent);
                        });
                    }catch(IOException ioe){
                        ioe.printStackTrace();
                    }
                }
            }
        });
        // 处理主题图标
        theme[0].setOnMousePressed(event -> theme[0].setFitHeight(16));
        theme[0].setOnMouseReleased(event -> {
            theme[0].setFitHeight(18);
            SingleSelectionModel selectionModel = tabPane.getSelectionModel();
            selectionModel.select(0);
            OSlider.setValue(255);
            HSlider.setValue(0);
            SSlider.setValue(0);
            VSlider.setValue(0);
            int stageQuantity = stages.size();
            if(stageQuantity != 1)
            {
                for(int i=1 ; i<stageQuantity ; i++)
                    stages.get(i).close();
            }

            if(themeFlag[0]) {
                cut.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/cut(1).png"))));
                photo.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/camera(1).png"))));
                file.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/picture(1).png"))));
                theme[0].setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/home(1).png"))));
                biggerIcon.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/bigger(1).png"))));
                info.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/info(1).png"))));

                pane.getStylesheets().remove(0);
                pane.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("prettify_white.css")).toExternalForm());

                middleSeparator.setStyle("-fx-opacity: 1");

                webText.setStyle("-fx-background-color: linear-gradient(to right, white, #e6e6e6)");

                RSlider.setValue(255);
                GSlider.setValue(255);
                BSlider.setValue(255);

                closeButton.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #e6e6e6; -fx-font-size: 15px");
                iconifiedButton.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #e6e6e6; -fx-font-size: 15px");

                titleBar.setStyle("-fx-background-color: #e6e6e6");

            }
            else {
                cut.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/cut.png"))));
                photo.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/camera.png"))));
                file.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/picture.png"))));
                theme[0].setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/home.png"))));
                biggerIcon.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/bigger.png"))));
                info.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("res/info.png"))));

                pane.getStylesheets().remove(0);
                pane.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("prettify.css")).toExternalForm());

                middleSeparator.setStyle("-fx-opacity: 0.11");

                webText.setStyle("-fx-background-color: linear-gradient(to right, black, #333333)");

                RSlider.setValue(0);
                GSlider.setValue(0);
                BSlider.setValue(0);

                closeButton.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #333333; -fx-font-size: 15px");
                iconifiedButton.setStyle("-fx-text-fill: #8a8d93; -fx-background-color: #333333; -fx-font-size: 15px");

                titleBar.setStyle("-fx-background-color: #333333");
            }
            themeFlag[0] = !themeFlag[0];
        });


        // 将界面设置为透明
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        System.out.println(Main.class.getClassLoader().getResource("res/icon.jpeg").toExternalForm());
        primaryStage.getIcons().add(
                new Image(Main.class.getClassLoader().getResource("res/icon.jpeg").toExternalForm())
        );
        primaryStage.show();
    }
}
