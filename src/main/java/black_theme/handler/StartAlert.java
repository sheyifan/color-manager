package black_theme.handler;

import black_theme.NodeHandler;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.DialogPane;

/**
 * 打开软件时弹出的信息窗口
 */

public class StartAlert implements NodeHandler {
    private Alert startAlert;
    private boolean[] themeFlag;
    @Override
    public void handleStyle() {
        DialogPane sDialogue = startAlert.getDialogPane();
        ObservableList<Node> ns = startAlert.getDialogPane().getChildren();
        // 如果主题颜色为黑色
        if(themeFlag[0]) {
            ns.get(1).setStyle("-fx-background-color: #333333 ; -fx-text-fill: linear-gradient(to right,#00fffc,#fff600)");
            ((ButtonBar)ns.get(2)).getStylesheets().add("button_style.css");
            sDialogue.setStyle("-fx-background-color: #333333 ; -fx-padding: 10px ; -fx-alignment: center");
            ns.get(0).setStyle("-fx-background-color: #333333");
        }
        // 如果主题颜色为白色
        else {
            ns.get(1).setStyle("-fx-background-color: #e6e6e6 ; -fx-text-fill: linear-gradient(to right,#333333,darkred)");
            ((ButtonBar)ns.get(2)).getStylesheets().add("button_style.css");
            sDialogue.setStyle("-fx-background-color: #e6e6e6 ; -fx-padding: 10px ; -fx-alignment: center");
            ns.get(0).setStyle("-fx-background-color: #e6e6e6");
        }

    }
    @Override
    public void handleEvent() {

    }
    @Override
    public void handleAttributes() {
        startAlert.setContentText("                    软件 : 调色取色器"+"\n"+"                    作者：佘一凡"+"\n"+"                    单位：苏州大学"+"\n"+"                    时间：2019.10"+"\n"+"                    版本 : 5.0");
        startAlert.setTitle("软件信息");
        startAlert.setResizable(false);
        startAlert.setX(300);
        startAlert.setY(250);
        startAlert.setHeaderText(null);
    }
    public StartAlert(Alert startAlert, boolean[] themeFlag) {
        this.startAlert = startAlert;
        this.themeFlag = themeFlag;
    }
}
