package black_theme.handler;

import black_theme.NodeHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.chart.PieChart;

public class MyChart implements NodeHandler {
    private PieChart pieChart;
    private ObservableList<PieChart.Data> chartData;
    @Override
    public void handleStyle() {
        chartData.get(0).getNode().setStyle("-fx-background-color: red");
        chartData.get(1).getNode().setStyle("-fx-background-color: green");
        chartData.get(2).getNode().setStyle("-fx-background-color: blue");
        pieChart.setStyle("CHART_COLOR_1: red ; CHART_COLOR_2: green ; CHART_COLOR_3: blue");
        pieChart.setLabelsVisible(false);
        pieChart.setLegendVisible(true);
        pieChart.setLegendSide(Side.LEFT);
        pieChart.setTitle("#000000");
        pieChart.setTitleSide(Side.TOP);
    }
    @Override
    public void handleEvent() {

    }
    @Override
    public void handleAttributes() {
        pieChart.setData(chartData);
    }
    public MyChart(PieChart pieChart, ObservableList<PieChart.Data> chartData) {
        this.pieChart = pieChart;
        this.chartData = chartData;
    }
}
